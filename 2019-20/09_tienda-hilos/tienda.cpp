#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include "barra.h"

struct TPila{
	int num_hilo;
	int count;
};

enum Options{
    create_hilos,
    show_hilos,
    exit_shop,
    OPTIONS	
};

const char*text[] = {
 "crear un hilo",
 "mostrar un hilo",
 "salir"
};

enum Options menu(){
	int op;

	do{

	 printf("OPCIONES\n");
	 printf("====================================\n");
	 for(int i=0; i<OPTIONS; i++)
		 printf("%i. %s\n", i, text[i]);

	 printf("Operacion a realizar: ");
	 scanf("%d", &op);

	}while(op<create_hilos || op>= OPTIONS);

	return (enum Options) op;

}

void* print_number(void* parameters){
	struct TPila* t = (struct TPila*) parameters;
	int i=0;
	int numero = 1;

	fputc(numero, stderr);
	
	for(i=0; i< t->count;i++){
		fputc(t->num_hilo, stderr);
	}
	return NULL;
}

int main(){

	int opcion;
	pthread_t thread1_id;
	pthread_t thread2_id;

	struct TPila thread1_args;
	struct TPila thread2_args;

	opcion = menu();


	switch(opcion){

	case 0:
		thread1_args.num_hilo = thread1_id;
		barra();
		pthread_create(&thread1_id, NULL, print_number, NULL);
		printf(" In main \nthread id = %li\n", thread1_id);
		break;
	case 1:
		break;
	case 2:
		pthread_join(thread1_id, NULL);
		break;
	}

	return 0;
}

