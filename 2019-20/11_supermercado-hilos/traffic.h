#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/sem.h>

void error(char* errorInfo){
	fprintf(stderr, "%s", errorInfo);
	exit(1);
}
// semaforo a cero

void DoSignal(int semid, int numSem){
	struct sembuf sops;
	sops.sem_num = numSem;
	sops.sem_op = 1;
	sops.sem_flg = 0;

	if(semop(semid,&sops, 1) == -1){
		perror(NULL);
		error("erroe al hacer señal");
	}
}
void DoWait(int semid, int numSem){
	struct sembuf sops;
	sops.sem_num = numSem;
	sops.sem_op = 1;
	sops.sem_flg = 0;

	if(semop(semid,&sops, 1) == -1){
		perror(NULL);
		error("erroe al hacer señal");
	}
}
void initSem(int semid, int numSem, int valor){
	if(semctl(semid,numSem, SETVAL, valor) < 0){
		perror(NULL);
		error("erroe al iniciar el semáforo");
	}
}
