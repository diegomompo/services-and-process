#include "menu.h"
#include "traffic.h"

const char *options[] = {
	"Crear cliente",
	"Cerrar el super"
};

enum Options menu(){
	int op;
	int traffic_menu;

	if((traffic_menu = semget(IPC_PRIVATE,1,IPC_CREAT | 0700))<0){
		perror(NULL);
		error("Semáforo: segmet");
	}
	initSem(traffic_menu,0,1);

	do{
		system("clear");

		system("toilet -fpagga - SUPERMERCADO");
		printf("\n""\n");

		DoWait(traffic_menu,0);
		sleep(2.5);
		puts("Entra en el menú\n");


		for(int i=0; i<OPTIONS; i++){
			printf(" 	%i. - %s. \n", i+1, options[i]);
		}

			DoWait(traffic_menu,0);
			sleep(2.5);
        		puts("Entra para introducir la opción\n");

			printf("\nOpción: ");
			scanf(" %i", &op);

			sleep(2.5);
			puts("Sale de introducir la opción\n");
        		DoSignal(traffic_menu, 0);

	}while(op<1 || op>OPTIONS);

	sleep(2.5);
	puts("Sale del menú\n");
	DoSignal(traffic_menu, 0);

	return (enum Options) --op;
}

