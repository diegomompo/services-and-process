#ifndef __STAK_H__
#define __STAK_H__

#include <pthread.h>

#define MAX_STACK 0x100

#define STACK_NOERROR 0
#define STACK_EMPTY   1
#define STACK_FULL    2
#define STACK_BKN     4

struct TStack{
	pthread_t data[MAX_STACK];
	int s;
};

#ifdef __cplusplus

extern "C"{

#endif
	int stack_status();
	void init (struct TStack *stack);
	void push (struct TStack *stack, pthread_t new_id);
	

