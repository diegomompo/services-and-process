#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

void *slowprintf (void *arg) {
	char *msg;
	int i;
	msg = (char *) arg;
	for ( i = 0 ; i < strlen (msg) ; i++ ) {
		printf ("%c", msg[i]);
		fflush (stdout );
		usleep (50000) ;
	}
}

int spawn (char* program, char** arg_list){

	pid_t child_pid;
	child_pid = fork();
	if(child_pid != 0)
		return child_pid;
	else{
		execvp (program, arg_list);
		fprintf(stderr, "An error occurred in execvp\n");
		abort();
	}
}

int main(int argc, char *argv[]){

        pthread_t  h1;
	pthread_t  h2;
        pthread_t  h3;
	pthread_t  h4;

        char *add = "Anadiendo ";
        char *comment = " Comentando ";
        char *file = "ficheros ";

	char * arg_list[] = {
		"git ",
		"add",
		"../../2019-20",
		NULL
	};
	char * arg_list2[] = {
		"git ",
		"commit",
		"-m",
		"'hilos'",
		NULL
	};

        spawn ("git", arg_list);

        pthread_create(&h1, NULL , slowprintf , (void *) add);
	pthread_join(h1, NULL);

        pthread_create(&h2, NULL , slowprintf , (void *) file);
	pthread_join(h2, NULL);

        spawn("git", arg_list2);

        pthread_create(&h3, NULL , slowprintf , (void *) comment);
	pthread_join(h3, NULL);

        pthread_create(&h4, NULL , slowprintf , (void *) file);
	pthread_join(h4, NULL);

        //spawn("git", arg_list2);

        printf("\n");


    return EXIT_SUCCESS;
}
