#ifndef __ARITMETICA_H__
#define __ARITMETICA_H__

#include <stdio.h>
#include <stdlib.h>
#include "aritmetica.cpp"

#ifdef __cplusplus

extern "C"{

#endif

int imprimir(){

	int numero1 = 5, numero2 = 3;
        int res_sumar, res_restar; 
		
	res_sumar = sumar(numero1, numero2);
        res_restar = restar(numero1, numero2);

        printf("El resultado de la suma es: %i\n", res_sumar);
        printf("El resultado de la restar es: %i\n", res_restar);

}
#ifdef __cplusplus
}
#endif


#endif
