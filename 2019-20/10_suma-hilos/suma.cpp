#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include "introducir.h"
#include "define_color.h"

#define N_THREADS 2
#define N 2

static int count = 0;
double sum = 0;
double res = 0;
double mul = 1;
double divi = 0;
double vec[N];

enum Operaciones{
	suma,
	resta,
	multiplicacion,
	division,
	OPERACIONES
};

const char *texto[] = {

	"sumar",
	"restar",
	"multiplicar",
	"dividir",
};

#include "menu.h"

void error(char* errorInfo){
	fprintf(stderr, "%s", errorInfo);
	exit(1);
}
//semaforo a creo
void DoSignal(int semid, int numSem){
	struct sembuf sops;
	sops.sem_num = numSem;
	sops.sem_op = 1;
	sops.sem_flg = 0;
	
	if(semop(semid, &sops, 1) == -1) {
		perror(NULL);
		error("Error al hacer Signal");
	}
}
void DoWait(int semid, int numSem){ // espera 
	struct sembuf sops;
	sops.sem_num = numSem;

	sops.sem_op = -1;
	sops.sem_flg = 0;

	if(semop(semid, &sops, 1) == -1) {
                perror(NULL);
                error("Error al hacer wait");
        }
}
void initSem(int semid, int numSem, int valor){ // inicio
	
	if(semctl(semid, numSem, SETVAL, valor) < 0){
		perror(NULL);
 		error("Error al iniciar el semaforo");
	}
}	

void *sumar (void *args){ // this function sum the two numbers
	double*  taskid;

	taskid = (double *) args;
	
	for(int i=0; i<N; i++){
		sum += vec[i];
	}

	pthread_exit(NULL);
}
void *restar(void *args){ // resta
	double* taskid;

	taskid = (double*) args;

	for(int i=0; i<N; i++){
		if(i==0){
		  res = vec[i];
		}
		else{
		  res -= vec[i];
		}
	}

	pthread_exit(NULL);
}
void *multiplicar(void *args){ // resta
	double* taskid;

	taskid = (double*) args;

	for(int i=0; i<N; i++){

		mul *= vec[i];	
	}

	pthread_exit(NULL);
}
void *dividir(void *args){ // resta
	double* taskid;

	taskid = (double*) args;

	for(int i=0; i<N; i++){
		if(vec[i] == 0){
		  printf("no se puede dividir entre 0");

		}
		else if(i==0){
		 	divi = vec[i];
		}
		else{
		     divi /= vec[i];
		}
	}

	pthread_exit(NULL);
}
void *slowprintf (void *arg) { // this function print a message
	char *msg;
	int i;
	msg = (char *) arg;
	for ( i = 0 ; i < strlen (msg) ; i++ ) {
		printf ("%c", msg[i]);
		fflush (stdout );
		usleep (50000) ;
	}
	printf("\n");
}

int main(int argc, char*argv[]){

	//thread
	pthread_t h1;
	pthread_t h2;
	pthread_t h3;
	pthread_t h4;
	pthread_t h5;
	pthread_t resultado;

	// normal variables
	int op;
	long taskids[N_THREADS];
	int  result;
	char *introducir = "Introduciendo numeros";
	char *calculando_sum  = "Calculando suma";
	char *calculando_res  = "Calculando resta";
	char *calculando_mul  = "Calculando multiplicacion";
	char *calculando_div  = "Calculando division";
	int semaforo;

	puts("Sincronización del semáforo");

	if((semaforo = semget(IPC_PRIVATE,1,IPC_CREAT | 0700))<0){
		perror(NULL);
 		error("Semáforo: semget");
	}
	initSem(semaforo,0,1);


	// call menu function
	
	DoWait(semaforo, 0);
	sleep(2.5);
	puts(YELLOW "Entra en la función menú" RESET_COLOR);
	op = menu();
	sleep(2.5);
	puts(YELLOW "Sale de la función menú" RESET_COLOR);
	DoSignal(semaforo,0);
	
	// call de vector function
	DoWait(semaforo, 0);
	sleep(2.5);
	puts(YELLOW "Entra en la función vector" RESET_COLOR);
	start_vector(vec);
	sleep(2.5);
	puts(YELLOW "Sale de la función vector" RESET_COLOR);
	DoSignal(semaforo,0);

	//call the message introduciendo function with thread
	DoWait(semaforo, 0);
	sleep(2.5);
	puts(YELLOW "Entra en la función mensaje introducir" RESET_COLOR);
	
	pthread_create(&h1, NULL, slowprintf, (void*) introducir);

	sleep(2.5);
	puts(YELLOW "Sale de la función mensaje introducir" RESET_COLOR);
	DoSignal(semaforo,0);

	//close the message function introduciendo function with thread
	pthread_join(h1,NULL);

	printf("\n");

	switch(op){ // switch the menu

		case 0:

		//call the function sum with thread

		DoWait(semaforo, 0);
		sleep(2.5);
		puts(YELLOW "Entra en la función suma" RESET_COLOR);
		pthread_create(&resultado, NULL, sumar, (void *) result);
		sleep(2.5);
       		puts(YELLOW "Sale de la función suma" RESET_COLOR);
		DoSignal(semaforo,0);

	
		//call the function message calculando suma with thread
		
		DoWait(semaforo, 0);
		sleep(2.5);
		puts(YELLOW "Entra en la función mensaje calculando suma" RESET_COLOR);
		pthread_create(&h2, NULL, slowprintf, (void*) calculando_sum);
		sleep(2.5);
       		puts(YELLOW "Sale de la función mensaje calculando suma" RESET_COLOR);
       		DoSignal(semaforo,0);


		//close the function sum with thread
		pthread_join(result,NULL);

		//close the function message calculando suma with thread
		pthread_join(h2,NULL);
	
		printf("\n");

		//print result


		DoWait(semaforo, 0);
		sleep(2.5);
		puts(YELLOW "Entra en la impresión de la suma" RESET_COLOR);
		printf("Suma: %.2lf\n", sum);
		sleep(2.5);
		puts(YELLOW "Sale de la impresión de la suma" RESET_COLOR);
       		DoSignal(semaforo,0);


		break;
		
		case 1:

		//call th function res with thread
		
		DoWait(semaforo, 0);
		sleep(2.5);
		puts(YELLOW "Entra en la función resta" RESET_COLOR);
		pthread_create(&resultado, NULL, restar, (void *) result);
		sleep(2.5);
		puts(YELLOW "Sale de la función resta" RESET_COLOR);
       		DoSignal(semaforo,0);

		//call the function calculando resta with thread
	
		DoWait(semaforo, 0);
		sleep(2.5);
		puts(YELLOW "Entra en la función mensaje calculando resta" RESET_COLOR);
		pthread_create(&h3, NULL, slowprintf, (void *) calculando_res);
		sleep(2.5);
       		puts(YELLOW "Sale de la función calculando resta" RESET_COLOR);
		DoSignal(semaforo,0);


		//close the function res with thread
		pthread_join(result, NULL);

		//close the function calculando resta with thread
		pthread_join(h3, NULL);

		printf("\n");

		//print result

		DoWait(semaforo, 0);
		sleep(2.5);
		puts(YELLOW "Entra en la impresión de la resta" RESET_COLOR);
		printf("Resta: %.2lf\n", res);	
		sleep(2.5);
		puts(YELLOW "Sale de la impresión de la resta" RESET_COLOR);
		DoSignal(semaforo,0);
		break;

		case 2:
		//call th function mul with thread
		
		DoWait(semaforo, 0);
		sleep(2.5);
		puts(YELLOW "Entra en la función multiplicar" RESET_COLOR);
		pthread_create(&resultado, NULL, multiplicar, (void *) result);
		sleep(2.5);
		puts(YELLOW "Sale de la función multiplicar" RESET_COLOR);
       		DoSignal(semaforo,0);

		//call the function calculando multiplicacion with thread
	
		DoWait(semaforo, 0);
		sleep(2.5);
		puts(YELLOW "Entra en la función mensaje calculando multiplicacion" RESET_COLOR);
		pthread_create(&h4, NULL, slowprintf, (void *) calculando_mul);
		sleep(2.5);
       		puts(YELLOW "Sale de la función calculando multiplicacion" RESET_COLOR);
		DoSignal(semaforo,0);

		//close the function mul with thread
		pthread_join(result, NULL);

		//close the function calculando multiplicacion with thread
		pthread_join(h4, NULL);

		printf("\n");

		//print result

		DoWait(semaforo, 0);
		sleep(2.5);
		puts(YELLOW "Entra en la impresión de la multiplicacion" RESET_COLOR);
		printf("Multiplicación: %.2lf\n", mul);	
		sleep(2.5);
		puts(YELLOW "Sale de la impresión de la multiplicacion" RESET_COLOR);
		DoSignal(semaforo,0);

		break;

		case 3:

		//call th function div with thread
		
		DoWait(semaforo, 0);
		sleep(2.5);
		puts(YELLOW "Entra en la función division" RESET_COLOR);
		pthread_create(&resultado, NULL, dividir, (void *) result);
		sleep(2.5);
		puts(YELLOW "Sale de la función division" RESET_COLOR);
       		DoSignal(semaforo,0);

		//call the function calculando division with thread
	
		DoWait(semaforo, 0);
		sleep(2.5);
		puts(YELLOW "Entra en la función mensaje calculando division" RESET_COLOR);
		pthread_create(&h5, NULL, slowprintf, (void *) calculando_div);
		sleep(2.5);
       		puts(YELLOW "Sale de la función calculando division" RESET_COLOR);
		DoSignal(semaforo,0);

		//close the function div with thread
		pthread_join(result, NULL);

		//close the function calculando division with thread
		pthread_join(h5, NULL);

		printf("\n");

		//print result

		DoWait(semaforo, 0);
		sleep(2.5);
		puts(YELLOW "Entra en la impresión de la division" RESET_COLOR);
		printf("Division: %.2lf\n", divi);	
		sleep(2.5);
		puts(YELLOW "Sale de la impresión de la division" RESET_COLOR);
		DoSignal(semaforo,0);

		break;
	}

	sleep(5);

	if(semctl(semaforo, 0, IPC_RMID) == -1) {
		perror(NULL);
		error("Semáforo borrado");
	}


	return 0;

}
