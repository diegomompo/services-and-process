#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

int thread_flag, seg, count = 0;

pthread_cond_t  thread_flag_cv;
pthread_mutex_t thread_flag_mutex;

void initialize_flag(){
	pthread_mutex_init (&thread_flag_mutex, NULL);
	pthread_cond_init (&thread_flag_cv, NULL);
	thread_flag = 0;	
}
void do_work(){
	printf("Han pasado %i segundos", seg);
}

void* thread_function(void* thread_flag){
	while(1){
		pthread_mutex_lock(&thread_flag_mutex);
		while(!thread_flag){
			pthread_cond_wait(&thread_flag_cv, &thread_flag_mutex);
			pthread_mutex_unlock(&thread_flag_mutex);
			do_work();
			usleep(0.0000001);
		}
		return NULL;
	}
}
void set_thread_flag(int flag_value){
	pthread_mutex_lock(&thread_flag_mutex);
	thread_flag = flag_value;
	pthread_cond_signal (&thread_flag_cv);
	pthread_mutex_unlock (&thread_flag_mutex);
}
int main(){
	pthread_t h1;
	pthread_t h2;

	initialize_flag();

	pthread_create(&h1, NULL, &thread_function, NULL);
	pthread_create(&h2, NULL, &thread_function, NULL);

	pthread_join(h1, NULL);
	pthread_join(h2, NULL);

	return EXIT_SUCCESS;
}
