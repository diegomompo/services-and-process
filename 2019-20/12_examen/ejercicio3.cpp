#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#define MAX 10
#define MAXL 500


pthread_mutex_t mutex;

struct TAlmacen {
  int ladrillos = 0;
  int recogidos = 0;
};

void error(char* errorInfo){
  fprintf(stderr, "%s", errorInfo);
  exit(1);
}
void DoSignal(int semid, int numSem){
  struct sembuf sops;
  sops.sem_num = numSem;
  sops.sem_op = 1;
  sops.sem_flg = 0;

  if(semop(semid, &sops, 1) == -1){
    perror(NULL);
    error("Error al hacer Signal");
  }
}

void DoWait(int semid, int numSem){
  struct sembuf sops;
  sops.sem_num = numSem;
  sops.sem_op = -1;
  sops.sem_flg = 0;

  if(semop(semid, &sops, 1) == -1){
    perror(NULL);
    error("Error al hacer wait");
  }
}
void initSem(int semid, int numSem, int valor){
  if(semctl(semid, numSem, SETVAL, valor) < 0){
    perror(NULL);
    error("Error al iniciar el semáforo");
  }
}


void RecogerLadrillos(struct TAlmacen *alm){

  int semaforo;
  int numero = 0;
  numero = rand() % MAX + 1;

  if((semaforo = semget(IPC_PRIVATE,1, IPC_CREAT | 0700)) <0){
    perror(NULL);
    error("Semáforo: semget");
  }

      initSem(semaforo,0,1);
      
  while(alm->recogidos < MAXL){
    DoWait(semaforo, 0);
    sleep(1);
    if(numero%2 == 0){
      alm->recogidos++;
    }
    else{
      alm->recogidos += 2;
    }


    printf("El trabajador con el id %i ha recogido %i ladrillos\n", pthread_self(), alm->recogidos);
    usleep(100000);

    sleep(1);

    DoSignal(semaforo, 0);
  }
}
void* funcion_fabrica(void * args){

  pthread_mutex_lock(&mutex);

  struct TAlmacen *alm = (struct TAlmacen*) args;
  
  while(alm->ladrillos < MAXL){
    alm->ladrillos++;
    usleep(10000);
    printf("La fabrica tiene. %i ladrillos\n", alm->ladrillos);
  }
  printf("La fabrica cerró \n");

  pthread_mutex_unlock(&mutex);

  return NULL;

}
void* obrero1(void * args){
  struct TAlmacen *alm = (struct TAlmacen*) args;

  RecogerLadrillos(alm);

  return NULL;
}
void* obrero2(void * args){
  struct TAlmacen *alm = (struct TAlmacen*) args;

  RecogerLadrillos(alm);

  return NULL;
}
void Cerrar_Fabrica(pthread_t th1, pthread_t th2){
  pthread_join(th1, NULL);
  pthread_join(th2, NULL);
}
int main(int argc, char* argv[]){
  struct TAlmacen alm;

  srand(time(NULL));

  pthread_t thread1_id;
  pthread_t thread2_id;
  pthread_t fabrica;

  pthread_mutex_init(&mutex, NULL);

  pthread_create(&thread1_id, NULL, &obrero1, &alm);
  pthread_create(&thread2_id, NULL, &obrero2, &alm);
  pthread_create(&fabrica, NULL, &funcion_fabrica, &alm);

  Cerrar_Fabrica(thread1_id, thread2_id);
  
  pthread_mutex_destroy(&mutex);

  return EXIT_SUCCESS;

}








