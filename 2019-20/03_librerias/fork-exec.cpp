#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

int spawn (char* program, char** arg_list){

	pid_t child_pid;
	child_pid = fork();
	if(child_pid != 0)
		return child_pid;
	else{
		execvp (program, arg_list);
		fprintf(stderr, "An error occurred in execvp\n");
		abort();
	}
}

int main(int argc, char *argv[]){

	char * arg_list[] = {
		"./",
		"lista",
		"modulos.cfg",
		NULL
	};
	char * arg_list2[] = {
		system(printf("Introduce el numero 1: ");
		system(scanf("%i", &numero1);
		system(printf("Introduce el numero 2: ");
		system(scanf("%i", &numero2);
		system(printf("La suma es %i ", numero1+numero2);
	};

	spawn("lista", arg_list);


	printf("done with main program\n");

    return EXIT_SUCCESS;
}
