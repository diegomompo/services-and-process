#ifndef __ARITMETICA_H__
#define __ARITMETICA_H__

#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#ifdef __cplusplus

extern "C"{
#endif
    const char **catalogo();

    int sumar(int numero1, int numero2);
    int restar(int numero1, int numero2);

#ifdef __cplusplus
}
#endif


#endif
