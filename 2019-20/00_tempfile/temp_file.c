#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

typedef int temp_file_handle;

temp_file_handle write_temp_file (char* buffer, size_t length){
	char temp_filename[] = "/tmp/tmp_file.XXXXXX";
	int fd = mkstemp (temp_filename);
	unlink (temp_filename);
	write(fd, &length, sizeof (length));
	write(fd, buffer, length);
	return fd;
}

char* read_temp_file (temp_file_handle temp_file, size_t* length){

	char* buffer;
	int fd = temp_file;
	lseek(fd, 0, SEEK_SET);
	read (fd, length, sizeof(*length));
	buffer = (char*) malloc (*length);
	read (fd, buffer, *length);
	close(fd);
	return buffer;
}
int main(int argc, char *argv[]){

	temp_file_handle archivo_anonimo;
	char mssg[] = "VIVA mi prima MONICA";
	size_t mssg_len = strlen(mssg);
	char *yield_mssg;
	char buffer;
	
	archivo_anonimo = write_temp_file (mssg, mssg_len);

	yield_mssg = read_temp_file (archivo_anonimo, &mssg_len);

	printf("El archivo becario contiene los siguiente: %s\n", yield_mssg);
	
	free(yield_mssg);

	return EXIT_SUCCESS;
}





